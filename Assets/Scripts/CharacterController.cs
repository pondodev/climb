using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    private enum PlayerState
    {
        Grounded,
        Jumping,
        WallSliding
    }

    private const float MinMoveDistance = 0.001f;
    private const float ShellRadius = 0.002f;
    
    [Header("Movement")]
    [SerializeField] private float moveSpeed = 1f;
    [SerializeField] private float jumpSpeed = 1f;
    [SerializeField] private float coyoteTime = 0.1f;
    [SerializeField] private float bufferTime = 0.1f;
    [SerializeField] private AnimationCurve jumpCurve;
    [SerializeField] private float gravityModifier = 1f;
    [SerializeField] private float wallFriction = 1f;

    private Rigidbody2D _rb;
    private PlayerState _state;
    private Vector2 _velocity;
    private Vector2 _targetVelocity;
    private ContactFilter2D _contactFilter;
    private RaycastHit2D[] _hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> _hitBufferList = new List<RaycastHit2D>(16);
    private float _minGroundNormalY = 0.65f;
    private Vector2 _groundNormal;
    private float _jumpTimer = 0.0f;
    private float _adjustedGravity => gravityModifier * jumpCurve.Evaluate(_jumpTimer);
    private Animator _animator;
    private bool _landed = false;
    private float _coyoteTimer = 0.0f;
    private Queue<float> _jumpQueue;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _velocity = new Vector2();
        _state = PlayerState.Jumping;
        
        _contactFilter.useTriggers = false;
        _contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        _contactFilter.useLayerMask = true;

        _animator = GetComponent<Animator>();
        _jumpQueue = new Queue<float>();
    }

    private void Update()
    {
        // animation stuff
        switch (_state)
        {
            case PlayerState.Jumping:
                _landed = false;
                _coyoteTimer += Time.deltaTime;
                break;
            
            case PlayerState.Grounded when !_landed:
                _landed = true;
                _animator.Play("Land");
                break;
            
            case PlayerState.Grounded:
                _coyoteTimer = 0.0f;
                break;
        }
        
        // input handling
        _targetVelocity = new Vector2();
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            // queue up next jump
            _jumpQueue.Enqueue(Time.timeSinceLevelLoad);
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (_velocity.y > 0)
            {
                _velocity.y *= 0.5f;
            }
        }

        _jumpTimer += Time.deltaTime;
        _targetVelocity = move * moveSpeed;
    }

    private void FixedUpdate()
    {
        switch (_state)
        {
            case PlayerState.Jumping when _coyoteTimer <= coyoteTime:
            case PlayerState.Grounded:
                if (_jumpQueue.Count > 0 &&
                    Time.timeSinceLevelLoad - _jumpQueue.Dequeue() <= bufferTime)
                {
                    _landed = false;
                    _animator.Play("Jump");
                    
                    _velocity.y = jumpSpeed;
                    _jumpTimer = 0.0f;
                }
                break;
            
            case PlayerState.WallSliding:
                // TODO: jump off wall
                break;
        }
        
        // handle physics
        _velocity += Physics2D.gravity * (_adjustedGravity * Time.fixedDeltaTime);
        _velocity.x = _targetVelocity.x * Time.fixedDeltaTime;
        
        _state = PlayerState.Jumping;

        Vector2 moveAlongGround = new Vector2(_groundNormal.y, -_groundNormal.x);
        
        Vector2 deltaPos = _velocity * Time.fixedDeltaTime;
        Vector2 move = moveAlongGround * deltaPos.x;
        Move(move, false);
        
        move = Vector2.up * deltaPos.y;
        Move(move, true);
    }

    private void Move(Vector2 move, bool yMovement)
    {
        float distance = move.magnitude;

        if (distance > MinMoveDistance)
        {
            // check for collisions
            int count = _rb.Cast(move, _contactFilter, _hitBuffer, distance + ShellRadius);
            _hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                _hitBufferList.Add(_hitBuffer[i]);
            }

            for (int i = 0; i < _hitBufferList.Count; i++)
            {
                Vector2 currentNormal = _hitBufferList[i].normal;
                // check if the player is grounded
                if (currentNormal.y > _minGroundNormalY)
                {
                    _state = PlayerState.Grounded;
                    if (yMovement)
                    {
                        _groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                // stops the player from intersecting with other colliders i think?
                // i wrote this so long ago it's hard to tell what i was thinking lol
                float projection = Vector2.Dot(_velocity, currentNormal);
                if (projection < 0)
                {
                    _velocity -= projection * currentNormal;
                }

                float modifiedDistance = _hitBufferList[i].distance - ShellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
        }
        
        _rb.position += move.normalized * distance;
    }
}
