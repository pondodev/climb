using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInController : MonoBehaviour
{
    private Material _material;
    private float _fadeValue = 0.0f;
        
    private void Start()
    {
        _material = GetComponent<SpriteRenderer>().material;
        _material.SetFloat("_Fade", 0.0f);
    }

    private void Update()
    {
        _fadeValue = Mathf.Lerp(_fadeValue, 1.0f, 0.025f);
        _material.SetFloat("_Fade", _fadeValue);
    }
}
