using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool GameRunning = false;

    [Header("Platform Spawning")]
    [SerializeField] private AnimationCurve difficultyCurve;
    [SerializeField] private float initialFallSpeed;
    [SerializeField] private float maxFallSpeed;
    [SerializeField] private float timeToMaxDifficulty;
    [SerializeField] private PlatformData platformData;
    
    private float _currentScore = 0.0f;
    private float _timeSinceStart;
    
    private void Start()
    {
        platformData.FallSpeed = initialFallSpeed;
    }

    private void Update()
    {
        if (!GameRunning) return;

        // calculate current point in difficulty curve
        _timeSinceStart += Time.deltaTime;
        float t = _timeSinceStart / timeToMaxDifficulty;
        t = difficultyCurve.Evaluate(t);
        platformData.FallSpeed = Mathf.Lerp(initialFallSpeed, maxFallSpeed, t);
    }
}
