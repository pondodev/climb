using UnityEngine;

[CreateAssetMenu(menuName = "Platform Data", fileName = "New Platform Data")]
public class PlatformData : ScriptableObject
{
    public float FallSpeed;
}
